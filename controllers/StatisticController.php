<?php


namespace app\controllers;


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use yii\db\Exception;
use yii\helpers\FileHelper;
use yii\web\Controller;
use app\services\StatisticService;


class StatisticController extends Controller
{
    private $statisticService;

    public function __construct($id, $module, $config = [], StatisticService $statisticService)
    {
        parent::__construct($id, $module, $config);
        $this->statisticService = $statisticService;
    }

    public function actionTest()
    {

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="demo.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
        //return 'Hello World';

    }


    public function actionExportData()
    {
        $month = 10;
        $year = 2015;
        $templateFile = Yii::getAlias("@app") . '\\' . 'excel-template' . '\\' . 'sokuhou_statistical.xlsx';
        // echo $templateFile.'<br>';
        $spreadSheet = IOFactory::load($templateFile);
        $outputFolder = Yii::getAlias('@webroot') . '/' . 'export';
        $outputFileName = 'statistical.xlsx';
        $this->writeDataToExcel($spreadSheet, $month, $year);
        $outputFile = $outputFolder . '/' . $outputFileName;

        $this->downloadFile($outputFile, $outputFileName);

    }

    public function actionExportChart(){
        $outputFolder = Yii::getAlias('@webroot') . '/' . 'export';
        $outputFileName = 'chart.xlsx';
        $spreadSheet = new Spreadsheet();
        $workSheet = $spreadSheet->getActiveSheet();
        $this->statisticService->createChart($workSheet,$spreadSheet,$outputFolder,$outputFileName);
        $outputFile = $outputFolder . '/' . $outputFileName;
        $this->downloadFile($outputFile,$outputFileName);
    }

    private function downloadFile($path, $outputFileName)
    {
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path, $outputFileName);

        }

    }


    private function data($month, $year)
    {

        $currentYear = $this->statisticService->getDataByMonth($month, $year);
        $previousYear = $this->statisticService->getDataByMonth($month, ($year - 1));
        return ['currentYear' => $currentYear, 'previousYear' => $previousYear];

    }

    public function actionData()
    {

        /*  $data = $this->statisticService->getCompanyNameData()['companyNameData'];
          foreach($data as $row){
              echo 'CompanyId = '.$row->companyId.'<br>';
              echo 'CompanyName = '.$row->companyName.'<br>';
              echo 'Data by chikuId => ';
              $dataByCompany = $this->statisticService->getDataByCompanyName($row->companyName,10,2015)['dataByCompany'];
              foreach($dataByCompany as $companyRow){
                  echo 'Sum = '.$companyRow->sum.'<br>';
                  $alphabet = $this->statisticService->findColumnName($companyRow->chikuId);
                  echo 'chikuId = '.$companyRow->chikuId.' - '.$alphabet.'<br>';
                  echo 'companyId = '.$companyRow->companyId.'<br>';
                  echo '<hr>';

              }
              echo '<hr>';
              echo '<hr>';
          }*/
        //  $dataCurrentYear = $this->statisticService->getChikuIdByCompany('日鉄住金高炉セメント株式会社',10,2015);
        /*   foreach($dataCurrentYear as $row){
               echo 'CompanyName = '.$row->companyName.'<br>';
               $columnName = $this->statisticService->findColumnNameS4($row->chikuId);
               echo 'ChikuId = '.$row->chikuId.'-'.$columnName.'<br>';
               echo 'Sum = '.$row->sum.'<br>';
               echo '<hr>';
           }*/
        /*  $dataPreviousYear = $this->statisticService->getChikuIdByCompany('日鉄住金高炉セメント株式会社',10,2014);
          echo '<hr>';
          echo '<hr>';
          foreach($dataPreviousYear as $row){
              echo 'CompanyName = '.$row->companyName.'<br>';
              $columnName = $this->statisticService->findColumnNameS4($row->chikuId,'F');
              echo 'ChikuId = '.$row->chikuId.'-'.$columnName.'<br>';
              echo 'Sum = '.$row->sum.'<br>';
              echo '<hr>';
          }*/
         $data = $this->data(10,2015);
          $dataCurrentYear = $data['currentYear'];
          $dataPreviousYear = $data['previousYear'];

          $i=0;


           foreach ($dataCurrentYear as $row){
               echo $row->chikuId.'<br>';
               echo $row->total;
               echo '<hr>';
           }
           /*
            echo '<hr>';
            echo '<hr>';
            for($i =0 ;$i<count($dataCurrentYear);$i++){
                // print_r( $dataPreviousYear[$i]);
                echo $dataCurrentYear[$i]['chikuId'].'<br>';
                echo $dataCurrentYear[$i]['month'].'<br>';
                echo $dataCurrentYear[$i]['total'];
                echo '<hr>';
            }
            echo '<hr>';
            echo '<hr>';
            $compareArr = array();
            $compareArr = $this->statisticService->calculatePercentTotalData($compareArr,$dataCurrentYear,$dataPreviousYear);
            for($i=0;$i<count($compareArr);$i++){
                echo 'i ='.$i.'<br>';
                $columnName = $this->statisticService->findColumnNameS4($compareArr[$i]['chikuId'],'F');
                echo $compareArr[$i]['chikuId'].' - '.$columnName.'<br>';
                echo $compareArr[$i]['percent'].'<br>';
                echo '<hr>';
            }*/



        // echo $data[0]['sum'];



}



    public function writeDataToExcel($spreadSheet,$month,$year){
        $outputFolder = Yii::getAlias('@webroot').'/'.'export';
        $outputFileName = 'statistical.xlsx';
        if(!is_dir($outputFolder)){
            mkdir($outputFolder,0777,true);
        }
        $data = $this->data($month,$year);
        $dataCurrentYear = $data['currentYear'];
        $dataPreviousYear = $data['previousYear'];
        $startSheetIndex = 0;
        //Write data to sheet 1 (index = 0)
        $spreadSheet->setActiveSheetIndex($startSheetIndex);
        $workSheet = $spreadSheet->getActiveSheet();
        //currentYear: column C c7->c10 - c12->c13 - c15->c20
        $this->statisticService->bindDataChikuId($dataCurrentYear,$workSheet,'C');
        //previous year with same month : column D
        $this->statisticService->bindDataChikuId($dataPreviousYear,$workSheet,'D');
        //対象年月(YYYY年MM月)
        $workSheet->setCellValue('C3','対象年月('.$year.'年'.$month.'月)');
        $workSheet->setCellValue('G22',15);
        $workSheet->setCellValue('H22',15);
        $startSheetIndex++;

        //WriteData to sheet2 (index = 1)
        $spreadSheet->setActiveSheetIndex($startSheetIndex);
        $workSheet = $spreadSheet->getActiveSheet();

        $companyName = $this->statisticService->getCompanyNameData()['companyNameData'];
        //Fill company name
        $this->statisticService->bindDataCompanyName($companyName,$workSheet,'B',1,$month,$year);
        $workSheet->setCellValue('A2','対象年月('.$year.'年'.$month.'月) 統計種類名　社別販売速報明細');
        //Fill data for each company name
        $this->statisticService->bindDataByCompanyName($workSheet,$month,$year,$companyName);
        $startSheetIndex++;

        //Write data to sheet 3(index=2)
        $spreadSheet->setActiveSheetIndex($startSheetIndex);
        $workSheet = $spreadSheet->getActiveSheet();
        $this->statisticService->bindDataCompanyName($companyName,$workSheet,'B',2,$month,$year);
        //対象年月(YYYY年MM月)　統計種類名　社別地区別販売高
        $workSheet->setCellValue('A2','対象年月('.$year.'年'.$month.'月) 統計種類名　社別地区別販売高');
        //Fill data for each company name
        $this->statisticService->bindDataByCompanyName($workSheet,$month,$year,$companyName);
        $startSheetIndex++;

        //Write data to sheet4(index=3)
        $spreadSheet->setActiveSheetIndex($startSheetIndex);
        $workSheet = $spreadSheet->getActiveSheet();
        //Fill company name
        $this->statisticService->bindDataCompanyName($companyName,$workSheet,'B',3,$month,$year);
        $this->statisticService->bindCompanyData($workSheet,$month,$year,$companyName);

        $writer = new Xlsx($spreadSheet);
        $outputFile = $outputFolder.'/'.$outputFileName;
        $writer->save($outputFile);




    }


}