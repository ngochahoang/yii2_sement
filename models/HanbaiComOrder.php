<?php


namespace app\models;


use yii\db\ActiveRecord;

class HanbaiComOrder extends ActiveRecord
{
    public $companyId;
    public $companyName;
    public static function tableName()
    {
        return 'hanbai_com_order';
    }
}