<?php

namespace app\services;

use app\models\HanbaiComOrder;
use app\models\SokuhouData;
use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Layout;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use yii\debug\panels\EventPanel;

class StatisticService
{
    public function getDataByMonth($month,$year){
        $sql = "select s.chikuId,s.month,sum(s.SUM) as total from (select sd.chikuId,sd.month,ci.companyName,sum(sd.data) as SUM from sokuhou_data sd inner join company_info ci 
                on sd.companyId=ci.companyId
                where statisticsType=1 and month(month)=:month and year(month)=:year
                GROUP BY sd.chikuId,sd.month,ci.companyName) as s
                group by s.chikuId,s.month";
        //$sql = 'SELECT * FROM customer WHERE status=:status';
        $sokuhoData = SokuhouData::findBySql($sql, [':month' => $month,':year' => $year])->all();
        return $sokuhoData;
    }

    //sheet0
    public function bindDataChikuId($data,$workSheet,$column){
        $startColumnIndex = 7;
        $endColumnIndex = 20;
        //c7->c10 - c12->c13 - c15->c20
        foreach($data as $row){
            if($startColumnIndex == 11 || $startColumnIndex == 14){
                $startColumnIndex++;
            }
            $workSheet->setCellValue($column.$startColumnIndex,$row->total);
            $startColumnIndex++;
            if($startColumnIndex > $endColumnIndex){
                $startColumnIndex=7;
                break;

            }
        }
    }

//sheet1,sheet2
    public function bindDataByCompanyName($workSheet,$month,$year,$companyName){
        $startRowIndex = 6;
        $endRowIndex = $startRowIndex + count($companyName) - 1;
        $columnName = 'E';
        while($startRowIndex >=6){
            //Get company name
            $companyName = $workSheet->getCell('B'.$startRowIndex)->getValue();
            //Find data by companyName
            $companyData = $this->getDataByCompanyName($companyName,$month,$year)['dataByCompany'];
            $this->setCellValue($companyData,$workSheet,$startRowIndex);
            $startRowIndex++;
            if($startRowIndex > $endRowIndex){
                //$startRowIndex=6;
                break;
            }

        }


    }

    public function findColumnName($chikuId){
        $startColumn = 'E';
        $startChikuId = 1;

        if($chikuId > 1 && $chikuId <= 4){
            $columnName = chr(ord($startColumn)+($chikuId-$startChikuId));
        }
        else if($chikuId == 5 || $chikuId == 6){
            $columnName = chr(ord($startColumn)+$chikuId);
        }
        else if($chikuId >= 7 && $chikuId <= 12){
            $columnName = chr(ord($startColumn)+($chikuId+1));
        }
        else if($chikuId == 13){
            $columnName = chr(ord($startColumn)+($chikuId+2));
        }
        else{
            $columnName=$startColumn;
        }

        return $columnName;


    }

    //Find column name only for sheet4
    public function findColumnNameS4($chikuId,$startColumn='E'){
     //   $startColumn = 'E';
        $startChikuId = 1;
        if($chikuId > 1 && $chikuId <= 4){
            $columnName = chr(ord($startColumn)+2*($chikuId-$startChikuId));
        }
        else if($chikuId == 5 || $chikuId == 6){
            $columnName = chr(ord($startColumn)+2*$chikuId);
        }
        else if($chikuId >= 7 && $chikuId <= 9){
            $columnName = chr(ord($startColumn)+2*($chikuId+1));
        }
        else if($chikuId >=10 && $chikuId<=12){
            $startColumn = $startColumn=='E'?'A':'B';
            $startChikuId=10;
            $columnName = 'A'.chr(ord($startColumn)+2*($chikuId-$startChikuId));
        }
        else if($chikuId == 13){
            $startChikuId=10;
            $columnName = 'A'.chr(ord($startColumn)+2*(2*($chikuId-$startChikuId)+2));
        }
        else{
            $columnName=$startColumn;
        }

        return $columnName;


    }

    //Sheet1
    public function getCompanyNameData(){
        $sql = "select hco.orderNum,hco.comCd,ci.companyId as companyId,ci.companyName as companyName from hanbai_com_order hco
                inner join company c on c.comCd=hco.comCd
                inner join company_info ci on ci.companyId=c.id
                group by hco.orderNum,hco.comCd,ci.companyId,ci.companyName 
                order by hco.orderNum ASC";
        $companyNameData = HanbaiComOrder::findBySql($sql)->all();
        return ['companyNameData' => $companyNameData];

    }
    //Sheet1,sheet2,sheet3
    public function bindDataCompanyName($companyName,$workSheet,$column,$sheetIndex,$month,$year){
        //6->22
        $startRowIndex = 6;
        $endRowIndex = $startRowIndex + count($companyName) - 1;//6+15-1=20
        //highestRow is current 合計 (Total) Row in template //20

        if($sheetIndex == 1){
            $highestRow = $workSheet->getHighestRow()-2;//21-2
            $totalRow = $workSheet->getHighestRow()-1;
        }
        if($sheetIndex == 2){
            $highestRow = $workSheet->getHighestRow()-6;//26-6=20//
            $totalRow = $workSheet->getHighestRow()-5;
        }
        if($sheetIndex == 3){
            $highestRow = ($workSheet->getHighestRow()/2)-2; //42/2-2
            $totalRow =($workSheet->getHighestRow()/2)-1;
        }
        //$totalRowIndex =$highestRow;
        $totalRowFomular = $workSheet->getCell('E'.$totalRow);//=SUM(E6:E20)
        $changeRowFormular = $workSheet->getCell('E22');
        foreach ($companyName as $row){
            //B6,B7,B...
            //Insert new row
            if($startRowIndex > $highestRow ){ //20>19

                $workSheet->insertNewRowBefore($startRowIndex,1);
              if($sheetIndex == 1){
                  $this->setFomular(1,$workSheet,$startRowIndex);
              }else if($sheetIndex == 2){
                  $this->setFomular(2,$workSheet,$startRowIndex);
              }
              else{
                  $this->setFomular(3,$workSheet,$startRowIndex);
              }



            }
            ////////////////////////////////
            if($sheetIndex == 3){
                $columnN = $this->calculateByColumn($row->companyName,$month,$year,3);//chikuId 3-4
                $workSheet->setCellValue('N'.$startRowIndex,$columnN);
                $columnT = $this->calculateByColumn($row->companyName,$month,$year,5);//chikuId 5-6
                $workSheet->setCellValue('T'.$startRowIndex,$columnT);
                $columnAH =  $this->sumDataChikuId($row->companyName,$month,$year);
                $workSheet->setCellValue('AH'.$startRowIndex,$columnAH);


            }
            $workSheet->setCellValue($column.$startRowIndex,$row->companyName);
            $startRowIndex++;
            //Set formular for 合計 (total) row in template if company name data has more than 14 records
            if($startRowIndex == $endRowIndex+1){
                $totalRowIndex = $startRowIndex;
                $totalRowFomular = str_replace($totalRow-1,$startRowIndex-1,$totalRowFomular);
                if($sheetIndex!=3){
                    $this->setFormulaForTotalRow($totalRowFomular,'E','V',$workSheet,$startRowIndex);
                }

                if($sheetIndex == 3){ //total row in last sheet
                    //set fomular for current year data
                    $this->setFormulaForTotalRow($totalRowFomular,'E','Y',$workSheet,$startRowIndex,true);
                   //set fomular for previous year data from F->Z
                    $this->setFormulaForTotalRowPreviousYear($workSheet,$month,$year,'F','Z',$startRowIndex);
                    //set fomular for column Y
                    $workSheet->setCellValue('Y'.$startRowIndex,'=SUM(Y6:'.'Y'.($startRowIndex-1).')');
                    //set fomular from column ZAA
                    $this->setCellValueFromAA($workSheet,$startRowIndex,'A');
                    //set result for comlumn N at total row(N21)
                    $columnN = $this->sumDataChikuIdByMonth($month,$year,3);
                    $workSheet->setCellValue('N'.$startRowIndex,$columnN);
                    //set result for comlumn T at total row(T21)
                    $columnT = $this->sumDataChikuIdByMonth($month,$year,5);
                    $workSheet->setCellValue('T'.$startRowIndex,$columnT);
                    //set result for column AH at total row (AH21)
                    $columnAH = $this->sumMonthDataByChikuId($month,$year);
                    $workSheet->setCellValue('AH'.$startRowIndex,$columnAH);



                }

            }

        }

         //continute to set formular for rows after 合計 (total) row in template if totalRowIndex has change
        //totalRowIndex change as insert new row to template
         if($sheetIndex == 2){
             if($totalRowIndex > $highestRow){
                 $startRowIndex++;
                 $chikuIdByMonth = $this->getDataByMonth($month,($year-1));
                 //setCellValue
                // $this->setCellValue($chikuIdByMonth,$workSheet,$startRowIndex);
                 $this->setCellValuePreviousYearData($chikuIdByMonth,$workSheet,$startRowIndex);
                 $startRowIndex++; // row 前年比
                 $this->setFormulaForChangeRow($changeRowFormular,'E','V',$workSheet,$startRowIndex,$totalRowIndex);
                 $startRowIndex++;//row 営業日数当り
                 $workSheet->setCellValue('C'.$startRowIndex,15);
                 $this->setFormulaForBusinessDayRow('E','V',$workSheet,$startRowIndex,$totalRowIndex);
                 $startRowIndex++;//前年営業日数当り
                 $workSheet->setCellValue('C'.$startRowIndex,15);
                 $this->setFormulaForBusinessDayRow('E','V',$workSheet,$startRowIndex,($startRowIndex-3));

             }

          }

    }

    private function calculateByColumn($companyName,$month,$year,$chikuId){
        //(current(3)+current(4))/(previous(3)+previous(4))
        $firstChikuId = $this->getCompanyDatabyChikuId($companyName,$month,$year,$chikuId);//3
        $secondChikuId = $this->getCompanyDatabyChikuId($companyName,$month,$year,$chikuId+1);//4
        $firstPreviousChikuId = $this->getCompanyDatabyChikuId($companyName,$month,$year-1,$chikuId);//3
        $secondPreviousChikuId = $this->getCompanyDatabyChikuId($companyName,$month,$year-1,$chikuId+1);//4
        $firstValue = $secondValue = $firstPrevious = $secondPrevious = 0;

        if($firstChikuId !== NULL){
            $firstValue = $firstChikuId->sum;
        }
        if($secondChikuId !== NULL){
            $secondValue = $secondChikuId->sum;
        }
        if($firstPreviousChikuId !== NULL){
            $firstPrevious = $firstPreviousChikuId->sum;
        }
        if($secondPreviousChikuId !== NULL){
            $secondPrevious = $secondPreviousChikuId->sum;
        }
        $currentValue = $firstValue+$secondValue;
        $previousValue = $firstPrevious+$secondPrevious;
        if($previousValue == 0){
            $previousValue = 1;
        }

        $percent = ($currentValue*100)/$previousValue;
        return round($percent,2);


    }



    private function setCellValuePreviousYearData($data,$workSheet,$rowIndex){
        for($i =0 ;$i<count($data);$i++){
            $columnName = $this->findColumnName($data[$i]['chikuId']);//E,T,...
            $workSheet->setCellValue($columnName.$rowIndex,$data[$i]['total']);

        }
    }

    private function setCellValueFromAA($workSheet,$startRowIndex,$startColumn){
        $endColumn = 'M';
        for($startColumn;$startColumn!=$endColumn; $startColumn = chr(ord($startColumn)+2)){
            //echo 'A'.$startColumn.'<br>';
           // =SUM(AG6:AG20)
            $workSheet->setCellValue('A'.$startColumn.$startRowIndex,'=SUM(A'.$startColumn.'6:A'.$startColumn.($startRowIndex-1).')');
        }
    }

    private function setFomular($sheetIndex,$workSheet,$startRowIndex){
        if($sheetIndex == 1 || $sheetIndex == 2){
            $previousColumnS = $workSheet->getCell('S'.($startRowIndex-1));
            $columnS = str_replace(($startRowIndex-1),$startRowIndex,$previousColumnS);
            $columnU = '=S'.$startRowIndex.'+T'.$startRowIndex;
            $columnL = '=SUM(J'.$startRowIndex.':K'.$startRowIndex.')';
            $columnI = '=SUM(G'.$startRowIndex.':H'.$startRowIndex.')';
            $workSheet->setCellValue('S'.$startRowIndex,$columnS);
            $workSheet->setCellValue('I'.$startRowIndex,$columnI);
            $workSheet->setCellValue('U'.$startRowIndex,$columnU);
            $workSheet->setCellValue('L'.$startRowIndex,$columnL);
        }
        else{
            $columnM = '=I'.$startRowIndex.'+K'.$startRowIndex;
            $columnS = '=O'.$startRowIndex.'+Q'.$startRowIndex;
            $previousColumnAG = $workSheet->getCell('AG'.($startRowIndex-1));
            $columnAG = str_replace(($startRowIndex-1),$startRowIndex,$previousColumnAG);
            $previousColumnAK = $workSheet->getCEll('AK'.($startRowIndex-1));
            $columnAK =  str_replace(($startRowIndex-1),$startRowIndex,$previousColumnAK);
            $workSheet->setCellValue('M'.$startRowIndex,$columnM);
            $workSheet->setCellValue('S'.$startRowIndex,$columnS);
            $workSheet->setCellValue('AG'.$startRowIndex,$columnAG);
            $workSheet->setCellValue('AK'.$startRowIndex,$columnAK);
        }
    }

    private function setFormulaForTotalRowPreviousYear($workSheet,$month,$year,$startColumn,$endColumn,$rowIndex){
        $dataCurrentYear = $this->getDataByMonth($month,$year);
        $dataPreviousYear = $this->getDataByMonth($month,($year-1));
        $i = 0;
        //F->Z
        $compareArr = array();
        $compareArr = $this->calculatePercentTotalData($compareArr,$dataCurrentYear,$dataPreviousYear);

        for($i=0;$i<count($compareArr);$i++){
            $columnName = $this->findColumnNameS4($compareArr[$i]['chikuId'],'F');
            $workSheet->setCellValue($columnName.$rowIndex,$compareArr[$i]['percent']);

        }


    }



    private function setFormulaForTotalRow($fomular,$startColumn,$endColumn,$workSheet,$rowIndex,$isSheet3=false){
        //fomular = SUM(E6:E21)
        if(!$isSheet3){
            for($startColumn;$startColumn!=$endColumn;$startColumn++){
                //=SUM(E6:E19)
                $calculate = str_replace('E',$startColumn,$fomular);
                $workSheet->setCellValue($startColumn.$rowIndex,$calculate);
            }
        }
        else{
            for($startColumn;$startColumn!=$endColumn;$startColumn = chr(ord($startColumn)+2)){
                //=SUM(E6:E19)
                $calculate = str_replace('E',$startColumn,$fomular);
                $workSheet->setCellValue($startColumn.$rowIndex,$calculate);
            }
        }

    }

    private function setFormulaForChangeRow($fomular,$startColumn,$endColumn,$workSheet,$rowIndex,$totalRowIndex){
        $previousRow = $rowIndex-1;
        $fomular = str_replace('21',$previousRow,$fomular);
        $fomular = str_replace('20',$totalRowIndex,$fomular);

        for($startColumn;$startColumn!=$endColumn;$startColumn++){
            $newFomular = str_replace('E',$startColumn,$fomular);
            $workSheet->setCellValue($startColumn.$rowIndex,$newFomular);
        }
    }

    //using for 前年営業日数当り row and 営業日数当り
    private function setFormulaForBusinessDayRow($startColumn,$endColumn,$workSheet,$rowIndex,$targetRow){
        for($startColumn;$startColumn!=$endColumn;$startColumn++){
            $calculate = '=ROUND(('.$startColumn.$targetRow.'/$C$'.$rowIndex.'),0)';
            $workSheet->setCellValue($startColumn.$rowIndex,$calculate);
        }
    }

    private function setCellValue($data, $workSheet,$rowIndex,$isSheet4=false,$startColumn='E'){
        if(!$isSheet4){
            foreach($data as $row){
                $columnName = $this->findColumnName($row->chikuId);//E,T,...
                $workSheet->setCellValue($columnName.$rowIndex,$row->sum);
            }

        }else{
            //determine startColumn is E or F
            $this->setCellValueForSheet4($data,$workSheet,$rowIndex,$startColumn);

        }

    }

    private function setCellValueForSheet4($data, $workSheet,$rowIndex,$startColumn='E'){
        if($startColumn == 'E'){
            foreach($data as $row){
                $columnName = $this->findColumnNameS4($row->chikuId);
                $workSheet->setCellValue($columnName.$rowIndex,$row->sum);
            }
        }else{
            foreach($data as $row){
                $columnName = $this->findColumnNameS4($row['chikuId'],'F');
                $workSheet->setCellValue($columnName.$rowIndex,$row['percent']);

            }
        }
    }

    //get data by company name (row)
    public function getDataByCompanyName($companyName,$month,$year){
        $sql = "select hco.orderNum,hco.comCd,sd.chikuId,sd.month,ci.companyId,ci.companyName as companyName,sum(sd.data) as sum from sokuhou_data sd 
                inner join company_info ci on sd.companyId=ci.companyId
                inner join company c on c.id=ci.companyId
                right join hanbai_com_order hco on hco.comCd=c.comCd
                where statisticsType=1 and month(month)=:month and year(month)=:year and ci.companyName=:companyName
                group by  hco.orderNum,hco.comCd,sd.chikuId,sd.month,ci.companyId,ci.companyName
                order by hco.orderNum ASC";
        $dataByCompany = SokuhouData::findBySql($sql,[':month' => $month,':year' => $year,':companyName' => $companyName])->all();
        return ['dataByCompany' => $dataByCompany];
    }

    public function getChikuIdByCompany($companyName,$month,$year){
        $sql = "select sd.chikuId,sd.month,ci.companyId,ci.companyName,sum(sd.data) as sum from sokuhou_data sd inner join company_info ci 
                on sd.companyId=ci.companyId
                inner join company c on c.id=ci.companyId
                where statisticsType=1 and month(month)=:month and year(month)=:year and ci.companyName=:companyName
                group by sd.chikuId,sd.month,ci.companyId,ci.companyName order by sd.chikuId asc";
        $sokuhouData = SokuhouData::findBySql($sql,['month'=>$month,'year'=>$year,'companyName'=>$companyName])->all();
        return $sokuhouData;
    }

    //sheet3
    public function bindCompanyData($workSheet,$month,$year,$companyName){
        $startRowIndex = 6;
        $endRowIndex = $startRowIndex + count($companyName) - 1;
        while($startRowIndex >=6){
            //Get company name
            $companyName = $workSheet->getCell('B'.$startRowIndex)->getValue();
            //Find (sql) data by companyName
            $dataCurrentYear = $this->getChikuIdByCompany($companyName,$month,$year);
            $dataPreviousYear = $this->getChikuIdByCompany($companyName,$month,$year-1);
            $this->setCellValue($dataCurrentYear,$workSheet,$startRowIndex,true);
            $compareArr = array();
            $compareArr = $this->calculatePercentData($compareArr,$dataCurrentYear,$dataPreviousYear);
            $this->setCellValue($compareArr,$workSheet,$startRowIndex,true,'F');
            $startRowIndex++;
            if($startRowIndex > $endRowIndex){
                //$startRowIndex=6;
                break;
            }

        }
        //$workSheet, $total
    }

    //By companyname
    public function calculatePercentData($compareArr,$dataCurrentYear,$dataPreviousYear){

        for($i=0;$i<count($dataCurrentYear);$i++){
            $value = ($dataCurrentYear[$i]['sum']/$dataPreviousYear[$i]['sum'])*100;
            $item = ['chikuId' =>$dataCurrentYear[$i]['chikuId'],'percent' => round($value,2)];
            // $compareArr[$data[$i]['chikuId']] = round($value,2);
            array_push($compareArr,$item);

        }
        return $compareArr;

    }

    //by chikuId
    public function calculatePercentTotalData($compareArr,$dataCurrentYear,$dataPreviousYear){
        for($i=0;$i<count($dataCurrentYear);$i++){
            $value = ($dataCurrentYear[$i]['total']/$dataPreviousYear[$i]['total'])*100;
            $item = ['chikuId' =>$dataCurrentYear[$i]['chikuId'],'percent' => round($value,2)];
            array_push($compareArr,$item);

        }
        return $compareArr;
    }

    public function getCompanyDatabyChikuId($companyName,$month,$year,$chikuId){
        $sql = "select ci.companyId,ci.companyName,sum(sd.data) as sum from sokuhou_data sd inner join company_info ci 
                on sd.companyId=ci.companyId
                inner join company c on c.id=ci.companyId
                where statisticsType=1 and month(month)=:month and year(month)=:year and ci.companyName=:companyName and sd.chikuId=:chikuId
                group by ci.companyId,ci.companyName order by sd.chikuId asc ";
        $sokuhouData = SokuhouData::findBySql($sql,['month'=>$month,'year'=>$year,'companyName'=>$companyName,'chikuId'=>$chikuId])->one();
        return $sokuhouData;
    }

    //sum chikuId by company name
    private function sumDataChikuId($companyName,$month,$year){
        $sumCurrentYear = $sumPreviousYear = 0;
        $currentYearData = $this->getDataByCompanyName($companyName,$month,$year)['dataByCompany'];
        $previousYearData = $this->getDataByCompanyName($companyName,$month,$year-1)['dataByCompany'];
        foreach($currentYearData as $row){
            $sumCurrentYear += $row->sum;
        }
        foreach($previousYearData as $row){
            $sumPreviousYear += $row->sum;
        }
        $precent = $sumCurrentYear*100/$sumPreviousYear;
        return round($precent,2);
    }
    //sum chikuId by Month and year
    private function sumDataChikuIdByMonth($month,$year,$chikuId){
        $firstChikuId = $this->getDataByChikuId($month,$year,$chikuId);//3
        $secondChikuId = $this->getDataByChikuId($month,$year,$chikuId+1);//4
        $firstPreviousChikuId = $this->getDataByChikuId($month,$year-1,$chikuId);//3
        $secondPreviousChikuId = $this->getDataByChikuId($month,$year-1,$chikuId+1);//4
        $firstValue = $secondValue = $firstPrevious = $secondPrevious = 0;

        if($firstChikuId !== NULL){
            $firstValue = $firstChikuId->total;
        }
        if($secondChikuId !== NULL){
            $secondValue = $secondChikuId->total;
        }
        if($firstPreviousChikuId !== NULL){
            $firstPrevious = $firstPreviousChikuId->total;
        }
        if($secondPreviousChikuId !== NULL){
            $secondPrevious = $secondPreviousChikuId->total;
        }
        $currentValue = $firstValue+$secondValue;
        $previousValue = $firstPrevious+$secondPrevious;
        if($previousValue == 0){
            $previousValue = 1;
        }

        $percent = ($currentValue*100)/$previousValue;
        return round($percent,2);
    }

    //get data  by specific chikuid and month and year //chikuId3+4 and 5+6
    private function getDataByChikuId($month,$year,$chikuId){
        $sql = "select s.chikuId,s.month,sum(s.SUM) as total from (select sd.chikuId,sd.month,ci.companyName,sum(sd.data) as SUM from sokuhou_data sd inner join company_info ci 
                on sd.companyId=ci.companyId
                where statisticsType=1 and month(month)=:month and year(month)=:year and chikuId=:chikuId
                GROUP BY sd.chikuId,sd.month,ci.companyName) as s
                group by s.chikuId,s.month";
        $sokuhouData = SokuhouData::findBySql($sql,['month'=>$month,'year'=>$year,'chikuId'=>$chikuId])->one();
        return $sokuhouData;
    }

    //calculate for column AH at sheet4 sum data all chikuId from 1->12 with specifiec year and month
    public function sumMonthDataByChikuId($month,$year){
        $dataCurrentYear = $this->getDataByMonth($month,$year);
        $dataPreviousYear = $this->getDataByMonth($month,$year-1);
        $totalCurrentYear = $this->calculateMonthDatabyChikuId($dataCurrentYear);
        $totalPreviousYear = $this->calculateMonthDatabyChikuId($dataPreviousYear);
        $percent = $totalCurrentYear*100/$totalPreviousYear;
        return round($percent,2);
    }

    private function calculateMonthDatabyChikuId($data){
        $total = 0;
        foreach ($data as $row){
            $total+=$row->total;
        }
        return $total;
    }

    public function createChart($workSheet,$spreadSheet,$outputFolder,$outputFileName){

        $seriesArr = array(
            array('2017'),
            array('2018'),
            array('2019')
        );
        $dataSeriesLabels = array();
        foreach($seriesArr as $seri){
            $element = new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, null, null, 1,$seri); // 2010
            array_push($dataSeriesLabels,$element);
        }

        /* $dataSeriesLabels = [
             new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, null, null, 1,['2017']), // 2010
             new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, null, null, 1,['2018']), // 2011
             new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, null, null, 1,['2019']), // 2012
         ];*/

        $xAxisValues = array('Apple','Mango','Melon','Strawberry');
        $xAxisTickValues = [
            //new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'Worksheet!$A$2:$A$5', null, 4), // Q1 to Q4
            new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, null, null, count($xAxisValues),$xAxisValues), // Q1 to Q4
        ];

        $seriesValuesArr = array(
            array('78','40','40','63'),
            array('30','60','17','30'),
            array('25','35','18','100')

        );
        $dataSeriesValues = array();
        foreach($seriesValuesArr as $seriValue){
            $element = new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, null, null, count($seriValue),$seriValue);
            array_push($dataSeriesValues,$element);
        }

        /*  $dataSeriesValues = [
              new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, null, null, 4,['78','40','40','63']),
              new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, null, null, 4,['30','60','17','30']),
              new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, null, null, 4,['25','35','18','100'])
          ];*/


        $series = new DataSeries(
            DataSeries::TYPE_LINECHART , // plotType
            DataSeries::GROUPING_STANDARD, // plotGrouping
            range(0, count($dataSeriesValues) - 1), // plotOrder
            $dataSeriesLabels, // plotLabel
            $xAxisTickValues, // plotCategory
            $dataSeriesValues          // plotValues
        );

        // Set the series in the plot area
        $plotArea = new PlotArea(null, [$series]);
        // Set the chart legend
        $legend = new Legend(Legend::POSITION_TOPRIGHT, null, false);

        $title = new Title('Demo');
        $yAxisLabel = new Title('Quantity');

        // Create the chart
        $chart = new Chart(
            'chart1', // name
            $title, // title
            $legend, // legend
            $plotArea, // plotArea
            true, // plotVisibleOnly
            0, // displayBlanksAs
            null, // xAxisLabel
            $yAxisLabel  // yAxisLabel
        );

        // Set the position where the chart should appear in the worksheet
        $chart->setTopLeftPosition('A7');
        $chart->setBottomRightPosition('H20');

        // Add the chart to the worksheet
        $workSheet->addChart($chart);
        $writer = new Xlsx($spreadSheet);
        $writer->setIncludeCharts(true);
        $callStartTime = microtime(true);
        $outputFile = $outputFolder.'/'.$outputFileName;
        $writer->save($outputFile);
    }

    public function createPieChart($workSheet,$spreadSheet,$outputFolder,$outputFileName){



// Set the Labels for each data series we want to plot
//     Datatype
//     Cell reference for data
//     Format Code
//     Number of datapoints in series
//     Data values
//     Data Marker
        $dataSeriesLabels1 = [
            new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, '{2011,2012}', null, 1), // 2011
        ];
// Set the X-Axis Labels
//     Datatype
//     Cell reference for data
//     Format Code
//     Number of datapoints in series
//     Data values
//     Data Marker
        $xAxisTickValues1 = [
            new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, "A1", null, 4), // Q1 to Q4
        ];
// Set the Data values for each data series we want to plot
//     Datatype
//     Cell reference for data
//     Format Code
//     Number of datapoints in series
//     Data values
//     Data Marker
        $dataSeriesValues1 = [
            new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, '{5,5,15,26}', null, 4),
        ];

// Build the dataseries
        $series1 = new DataSeries(
            DataSeries::TYPE_PIECHART, // plotType
            null, // plotGrouping (Pie charts don't have any grouping)
            range(0, count($dataSeriesValues1) - 1), // plotOrder
            $dataSeriesLabels1, // plotLabel
            $xAxisTickValues1, // plotCategory
            $dataSeriesValues1          // plotValues
        );

        // Set up a layout object for the Pie chart
        $layout1 = new Layout();
        $layout1->setShowVal(true);
        $layout1->setShowPercent(true);

        // Set the series in the plot area
        $plotArea1 = new PlotArea($layout1, [$series1]);
        // Set the chart legend
        $legend1 = new Legend(Legend::POSITION_RIGHT, null, false);

        $title1 = new Title('Test Pie Chart');

        // Create the chart
        $chart1 = new Chart(
            'chart1', // name
            $title1, // title
            $legend1, // legend
            $plotArea1, // plotArea
            true, // plotVisibleOnly
            0, // displayBlanksAs
            null, // xAxisLabel
            null   // yAxisLabel - Pie charts don't have a Y-Axis
        );

        // Set the position where the chart should appear in the worksheet
        $chart1->setTopLeftPosition('A7');
        $chart1->setBottomRightPosition('H20');

        // Add the chart to the worksheet
        $workSheet->addChart($chart1);

        $writer = new Xlsx($spreadSheet);
        $writer->setIncludeCharts(true);
        $callStartTime = microtime(true);
        $outputFile = $outputFolder.'/'.$outputFileName;
        $writer->save($outputFile);
    }







}